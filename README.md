Copyright (C) 2018 Vladimir Markovic
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and limitations under the License.

# SpanClickTextView 
## Makes it easy to have custom styled clickable links within text with custom defined navigation / action options.

* All styling and navigation/action options can be specified from xml as well as set in code.
* Links are specified within text using start and delimiter indicators. Default are {{ and }}, though the can be custom defined. i.e.:
			android:text="Click on the link to open {{Google}}, {{Gmail}}, {{Google Plus}}, {{Likned In}}, {{Google Maps}}"
* Attributes can be specified  using one of three was:
	1. as reference - specifying a reference to an array (of strings, booleans or colors depending on the attribute) - each item in the array will correspond to each link. I.e.: app:spanTitle="@ array/screenTitles"
	
			<array name="screenTitles">
				<item>Google<item>
		    	<item>Google<item>
	        	<item>Google<item>
	        	<item>LinkedIn<item>
	        	<item>Google<item>
			</array>
		
	2. as a single value (of strings, booleans or colors depending on the attribute) - which will apply to all links. i.e.
			app:spanTitle="Google"
	3. as a string of delimited values and optionally specifying indices to which it applies. It indices are not specified, it will apply to corresponding link in terms of index where it appears. Default delimiter is |. Default format for specifying indices is =index1, index2, ... or =index_range, index, index_range. Index range default format is indexStart-indexEnd. I.e. =0-3,6. I.e.:
			app:spanTitle="Google, Google, Google, Linked In, Google" 
or
	app:screenTitle="Google=0-2,4|Linked In=3"

## Attributes:

### Attributes specifying navigation on span click:

* spanUrl (string|reference) - for specifying urls corresponding to the links

* spanDestination (string|reference) - for specifying navigation options for the specified urls. It can be:
	1. opening the URL in Chrome Custom Tab (specified as "cct" or "chrome custom tabs") 
	2. opening the URL in an external browser (specified as "browser" or "external browser")
	3. passing the URL (together with screen title) if specified to the custom defined activity. Common application would be to open the URL within the custom WebView hosted within the activity (specified as "my.application.package.MyCustomActivity")
	4. defining a custom action by setting the OnSpanClickListener.

* spanTitle (string|reference) - for setting the title when opening a new screen, such as when specifying a custom activity to open.

* spanChromeCustomTabsBackgroundColor (string|reference|color)

### Attributes for styling spans (usually as links):

* spanBackgroundColor (string|reference|color)
* spanTextColor (string|reference|color)
* spanTextSize (string|reference|dimension)
* spanTextBold (string|reference|boolean). Default is true.
* spanTextUnderlined (string|reference|boolean). Default is true.

### Attributes for indicating where clickable span starts and ends:

* spanStartIndicator (string). Default is "{{"
* spanEndIndicator (string). Default is "}}"

### Attributes for passing data to activity via intent:

* intentExtraUrlTag (string). For passing url to activity.
* intentExtraTitleTag (string). For passing screen name to activity

When specifying multiple colors as a string of delimited values, hash can be omitted from the first one.
